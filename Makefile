all:
	@echo Give package names to perform $(ACTION) builds of those packages
	@echo Use 'make all-clients' to build everything in dependency order.

.PHONY: all

# Set these variables to appropriate values for the rebuild you're doing.
boost_version := 1.83
# The Fedora release that the rebuilds are for.
#releasever := 39
releasever := 40
# This is the side-tag that will be used for SCRATCH and BUILD targets.
# Leave blank to build directly in rawhide, not a side-tag.
# sidetag := f$(releasever)-boost
sidetag :=

target := $(if $(sidetag),$(sidetag),f$(releasever))
build_target := $(if $(sidetag),$(sidetag),f$(releasever)-build)

# Set to ID of a recent build from
# https://koji.fedoraproject.org/repos/$(build_target)/?C=M;O=D
# If left blank then the latest build will be used.
# build_id := 1074358

# Where to find the repo that LOCAL builds will use for the buildroot
baserepos := https://koji.fedoraproject.org/repos/$(build_target)

# This will be added to the spec files of packages being rebuilt.
comment := Rebuilt for Boost $(boost_version)
# This is the directory where local dist-git repos are checked out.
scmdir := $(HOME)/src/fedora-scm
# This is an identifying tag used to distinguish different rebuilds.
id := boost$(subst .,,$(boost_version))

ifdef USE_CURL
FETCHURL := curl -O
else
FETCHURL := wget
endif

# This is the default action that "make foo" will do,
# which should be  one of: LOCAL, COPR, SCRATCH, PUSHIT, BUILD
ACTION := $(shell test -f ACTION && cat ACTION || echo SCRATCH)

# Echo the default action used for the current directory.
print-action:
	@echo $(ACTION)

.PHONY: print-action

# Define this to allow BUILD to be done without a successful SCRATCH first.
ifdef WITHOUT_SCRATCH
_WITHOUT_SCRATCH := :
else
_WITHOUT_SCRATCH :=
endif

# By default we do SCRATCH builds on these architectures:
scratcharches := x86_64 aarch64
scratchbuild := fedpkg scratch-build --arches $(scratcharches) --target $(target) --fail-fast


# For a typical boost update in rawhide we don't need to rebuild devel-clients.
# These only depend on Boost headers, and so are not affected by changing the shared libs.
# If rebuilding them is desired, find all SRPMs to rebuild using:
# find_devel_clients := sudo dnf -q repoquery -s --releasever=rawhide --whatrequires boost-devel --repo=fedora | sed -n 's/-[[:digit:]].*//p' | grep -v '^boost$$' | sort -u
devel-clients: brial CGAL cocoalib cpp-hocon cpprest curlpp dolphin-connector dyninst e-antic eclib flann flowcanvas folly freetdi-gala gazebo gecode gnuradio hpx imath kdepimlibs kea kf5-akonadi-server kf5-kimap kf5-kmime kf5-libktorrent leatherman libASL libclaw libebml libime libkml libkolabxml liblas libmatroska libnest2d libosmium libyui libzypp luabind mapnik mdds metslib milia octave-iso2mesh ogre ompl orocos-bfl permlib player polymake pythran rb_libtorrent rcssserver3d simspark snapper source-highlight source-highlight-qt supercollider sympol thrift urg vigra votca vtk websocketpp xmms2 xylib

# This is the list of packages that link to libboost_*.so.* which must be rebuilt
# when updating to a new Boost release.
# A build must be done (or at least attempted) for everything in this list.
# There might be failures due to missing dependency info (see root.log for failed build)
# which means the dependencies for the package (see below) need to be updated,
# and the build retried.
# Find the current list of packages using this command:
find_all_clients := sudo dnf -q repoquery -s --releasever=rawhide --whatrequires libboost\* --repo=fedora | sed -n 's/-[[:digit:]].*//p' | grep -v '^boost$$' | sort -u
# These packages have no dependency on libboost_*.so recorded in the RPM
# but they *do* depend on it (maybe just for the %check during the build).
extra_clients := CGAL

all_clients := 0ad airinv airrac airtsp alchemyquest anyterm aqsis asymptote auryn bastet bear-factory blender boost-http-server botan btbuilder calamares codeblocks collada-dom colobot condor cpp-hocon credentials-fetcher cryfs cryptominisat csdiff dmlite dolfin domoticz dssp dyninst easystroke eclib elements elements-alexandria espresso fastnetmon fbthrift fcitx5-chinese-addons Field3D FlightCrew folly freeopcua galera gazebo gearmand gfal2-python glob2 glogg gnucash gnuradio gource gpick grive2 gr-osmosdr gr-rds guitarix heaptrack hokuyoaist hugin imath inkscape innoextract kea kig leatherman ledger lgogdownloader libASL libcamera-apps libcmis libime libixion libkolabxml liblas liborcus libphonenumber libpst libreoffice librime libyui-gtk libyui-mga-gtk libzypp logstalgia lucene++ luminance-hdr lunchbox luxcorerender lv2-c++-tools Macaulay2 maeparser mapnik mcrouter mir mkvtoolnix mmseq mupen64plus ncmpcpp nextpnr ogre ompl OpenImageIO openscad openshadinglanguage opentrep openvdb osm2pgsql osmium-tool pcl pdns pdns-recursor poedit pokerth povray prusa-slicer pulseview python3-exiv2 python-gattlib python-graph-tool python-mapnik python-tdlib radiotray-ng rb_libtorrent rmol rstudio scummvm-tools sevmgr SFCGAL simcrs simfqt simspark slic3r smesh snapper source-highlight source-highlight-qt sourcextractor++ stdair stp supercollider supertux sympol systemtap trademgen travelccm trellis trojan uhd usd vigra votca vsomeip3 vtk wesnoth wsjtx zswap-cli  IQmol caffe kopeninghours libindi mstflint tvlsim

all-clients: $(all_clients) $(extra_clients)

.PHONY: devel-clients all-clients

ifneq ($(CHECK),)
$(info Running dnf repoquery ...)
repoquery_result := $(shell $(find_all_clients))
diff1 := $(filter-out $(all_clients),$(repoquery_result))
diff2 := $(filter-out $(repoquery_result),$(all_clients))
ifeq ($(diff1)$(diff2),)
$(info OK)
else
$(warning Warning: List of all-clients does not match latest repoquery result.)
$(info Missing from all-clients: $(diff1))
$(info Not in repoquery results: $(diff2))
endif
endif

# The following phony targets (roots, leaves etc.) are just for convenience,
# to support running self-contained subsets of the full package list.
# N.B. not all packages from the all-clients list are repeated in one of
# these targets, so do not rely on commands like 'make roots all-leaves'
# to build everything. Use 'make all-clients' at ensure nothing is missed.
# These might also be become inaccurate as packages evolve, e.g. mkvtoolnix seems
# to no longer be used by anything in all-clients, so is a leaf not a root now.

# TODO: Add a check that all these prerequisites are included in all-clients.
# TODO: Otherwise running something like 'make roots' will do unnecessary rebuilds.

roots1: 0ad FlightCrew SFCGAL anyterm aqsis bastet bear-factory botan codeblocks collada-dom condor csdiff

roots2: cryptominisat dyninst easystroke elements folly

roots3: galera gfal2-python glob2 glogg glom gource gpick hokuyoaist hugin kea kig imath

roots4: leatherman ledger libcmis libime libixion liblas libphonenumber libpst lucene++ luminance-hdr

roots5: mkvtoolnix mmseq ncmpcpp ogre ompl openvdb osm2pgsql pcl pdns pdns-recursor pokerth povray

roots6: rb_libtorrent scummvm-tools simspark slic3r source-highlight supertux sympol smesh

roots7: trellis uhd vigra wesnoth

roots: roots1 roots2 roots3 roots4 roots5 roots6 roots7

# only depend on roots, used by other things
#level2: Field3D gnuradio stdair mapnik dmlite
level2: Field3D stdair liborcus dmlite elements-alexandria
# level3: airrac airtsp sevmgr simfqt travelccm libreoffice
level3: OpenImageIO airrac airtsp sevmgr simfqt travelccm
#level4: blender rmol gr-osmosdr
level4: rmol
# level5: airinv gqrx
level5: airinv
level6: simcrs

# these depend on nothing, nothing depends on them
leaves: alchemyquest dolfin domoticz dssp eclib espresso fastnetmon freeopcua gnucash grive2 guitarix heaptrack inkscape innoextract lgogdownloader libASL libcamera-apps libkolabxml libyui-gtk libyui-mga-gtk libzypp logstalgia lunchbox lv2-c++-tools maeparser mir mupen64plus osmium-tool pulseview python3-exiv2 python-gattlib python-tdlib radiotray-ng rstudio snapper supercollider trojan usd vsomeip3 vtk wsjtx zswap-cli

# only depend on roots, not used by anything else
level2-leaves: Macaulay2 fcitx5-chinese-addons gazebo gearmand gr-rds librime luxcorerender mapnik mcrouter nextpnr openscad openshadinglanguage opentrep poedit prusa-slicer source-highlight-qt sourcextractor++ stp systemtap

# depend on level2, not used by anything else
level3-leaves: python-mapnik simcrs trademgen travelccm

# not used by anything else
all-leaves: leaves level2-leaves level3-leaves

# These builds take a very long time
slow: pcl libreoffice python-graph-tool

.PHONY: roots roots1 roots2 roots3 roots4 roots5 roots6 roots7 level2 level3 level4 level5 level6 leaves level2-leaves level3-leaves all-leaves slow

all_deps := $(shell for i in $(all_clients); do sed -n "/^$$i:/{s/^$$i://;s/#.*//;p}" $(firstword $(MAKEFILE_LIST)); done | xargs -n1 | sort -u)
bad_deps := $(filter-out $(all_clients) FTBFS scratcharches=%, $(all_deps))
ifneq ($(bad_deps),)
$(info These packages are listed as dependencies but do not need to be rebuilt: $(bad_deps))
$(error Remove those bad dependencies to continue)
endif

boost:
	@echo "$@: Do not build boost using this makefile!"
	@false

# When a package is known to "fail to build from source" add FTBFS as its prerequisite
FTBFS:
	@false


# Define the build dependencies of packages as Make prerequisites ...

0ad: #
CGAL: #
Coin3: #
Field3D: imath #
FlightCrew: #
FlightGear: # SimGear
FlightGear-Atlas: # SimGear
IQmol:
LuxRender: OpenImageIO #
Macaulay2: source-highlight #
MyPasswordSafe: #
OpenImageIO: Field3D openvdb
OpenSceneGraph: collada-dom
QuantLib: #
SFCGAL: #
SimGear: #
SkyX: ogre
YafaRay: #
# abiword #
adobe-source-libraries: #
airinv: stdair airrac sevmgr rmol
airrac: stdair
airtsp: stdair
akonadi: #mariadb
alchemyquest: #
anyterm: #
apt-cacher-ng: #
aqsis:
asc: #
asio: #
assimp: #
autowrap: #
avogadro: #
barry: #
bastet: #
bear-factory:
bibletime: #
blender: OpenImageIO Field3D
blobby: #
boost-gdb-printers:
botan:
brial: #
btbuilder: #
calamares: # yaml-cpp
calligra: akonadi
cclive:
cegui: ogre
ceph: #
clementine: #
clucene: #
codeblocks:
collada-dom: #
colobot:
condor: # CMake configuration issues
console-bridge: #
cpp-hocon: leatherman
crrcsim:
cryptominisat: # src/ccnr.h:103:24: error: 'uint32_t' has not been declared
csdiff: #
csound:
cups-filters: #
curlpp:
cvc4: #
cyphesis: #
cryfs: # /src/cpp-utils/thread/LeftRight.h:54:24: error: 'logic_error' is not a member of 'std'
dans-gdal-scripts:
davix: #
device-mapper-persistent-data: #
diet: #
dmlite: # davix #
dmlite-plugins-s3: dmlite
dolphin-connector: #
dolfin:
domoticz:
dssp: #
dynafed: dmlite
dyninst: scratcharches=x86_64 #
easystroke: #
eclib: #
edb: #
ekiga: #
elements: #
elements-alexandria: elements
ember:
enblend: vigra
engrid: CGAL #
erlang-basho_metrics: #
espresso:
exempi: #
facter: cpp-hocon leatherman
fastnetmon: # /usr/include/capnp/blob.h:176:74: error: no match for 'operator==' (operand types are 'const char*' and 'kj::StringPtr')
fawkes: pcl gazebo
fb303: folly fbthrift
fbthrift: folly # /builddir/build/BUILD/fbthrift-2022.07.11.00/thrift/compiler/generate/json.cc:45:9: error: 'uint8_t' was not declared in this scope
fbzmq: folly fbthrift
fcitx5-chinese-addons: libime
fgrun: SimGear
fityk: #
flamerobin: #
flann: #
flowcanvas: source-highlight #
folly: #  /builddir/build/BUILD/folly-2022.07.11.00/folly/system/AtFork.cpp:73:26: error: 'invalid_argument' is not a member of 'std'
freecad:
freeopcua:
fritzing: #
fts: #
funguloids: ogre
fuse-encfs: #
# galera: # The following tests FAILED: 2 - gu_tests++ (Failed)
galera: #
gappa: #
gazebo: ogre
gearmand: #mariadb
gecode: #
gfal2-python: #
glob2: #
glogg: #
gnote: #
gnucash: # /usr/include/glib-2.0/glib/gstring.h:72:5: error: ignoring return value of 'g_string_free_and_steal' declared with attribute 'warn_unused_result' [-Werror=unused-result]
gnuradio: uhd #
gource: #
gpick: scratcharches=x86_64 #
gpsdrive: #
gqrx: gnuradio gr-osmosdr
gr-air-modes: gnuradio
gr-fcdproplus: gnuradio
gr-funcube: gnuradio
gr-hpsdr: gnuradio
gr-iio: gnuradio
gr-iqbal: gnuradio
gr-osmosdr: gnuradio
gr-rds: gnuradio
grfcodec: #
grive2: #
gromacs: #
guitarix: #
hamlib: #
heaptrack: #
highlight: #
hokuyoaist: #
hpx:
hugin: #
imath: #
inkscape: # ./src/ui/widget/selected-style.cpp:111:6: error: statement-expressions are not allowed outside functions nor in template-argument lists
iwhd: mongodb mongo-cxx-driver
k3d: source-highlight
kcm_systemd: #
kdepim: kdepimlibs
kdepim-runtime: kdepimlibs libkolab libkolabxml
kdepimlibs: akonadi
kdevelop: #
kdevplatform: #
kea: #
kf5-kactivities: #
kgraphviewer: #
kicad: source-highlight #
kig: #
kmymoney: akonadi
kpilot: akonadi
ktorrent: kdepimlibs libktorrent
ladish: source-highlight
launchy: #
leatherman: #
ledger:  #
libASL: #
libabw:
libcdr:
libclaw: #
libcmis:
libcutl: #
libe-book:
libepubgen: librevenge
libetonyek: #
libflatarray: scratcharches=x86_64 #
libftdi: #
libgltf: #
libint2: #
libixion: #
libkindrv: #
libkml: #
libkni3: #
libkolab: kdepimlibs libkolabxml
libkolabxml: # kdepimlibs xsd # swig # libcutl
libktorrent: #
liblas:
libmspub: #
libmwaw: #
libndn-cxx: # https://bugzilla.redhat.com/show_bug.cgi?id=1842303
libodb-boost: #
libodfgen: #
libopenraw: exempi
libopkele: #
liborcus: libixion
liborigin2:
libosmium:
libpagemaker: #
libpst: #
libpwiz: #
librecad: #
libreoffice: libcmis liborcus vigra libphonenumber # libcdr libe-book libetonyek libgltf libmspub libmwaw libodfgen libpagemaker librevenge mdds
librevenge: #
librime: # yaml-cpp
librvngabw: #
libvisio: #
libwps: #
libyui: #
libyui-bindings: #
libyui-gtk: #
libyui-mga-gtk: libyui-gtk #
libyui-ncurses: #
libyui-qt: #
libzypp: # The following tests FAILED: 39 - RpmPkgSigCheck_test (Failed)
#licq: # retired
logstalgia: #
lrslib: #
luabind: #
lucene++: #
luminance-hdr: #
luxcorerender: OpenImageIO
lv2-c++-tools:
lv2-sorcer: #
lyx:
mapnik: SFCGAL #
mariadb: #
mbox2eml: #
mcrouter: fbthrift
mdds: #
meshmagick: ogre
meson: scratcharches=x86_64
meson: source-highlight #
mesos : zookeeper
metslib :
milia: #
minion: #
mir:
mkvtoolnix: #
mlpack: #
mmseq: #
mongodb: #
mongo-cxx-driver: mongodb
monotone: #
mrpt: pcl
mstflint:
mygui: ogre
ncbi-blast+: #
ncmpcpp: #
nemiver: #
nextpnr: trellis # 
nodejs-mapnik: mapnik
nodejs-mapnik-vector-tile: mapnik
normaliz: #
nss-gui: source-highlight
nuspell: rdkit #
ogre: imath #
ompl: #
openms:
openoffice.org-diafilter: libcmis liborcus libreoffice
openscad:
openshadinglanguage: OpenImageIO imath
opentrep: #
openvdb:
osm2pgsql: #
osmium-tool: #
oyranos:
paraview: #
pcb2gcode: #
pcl: #
pcp-pmda-cpp: #
pdfedit: #
pdns: #
pdns-recursor: #
percolator: libcutl #
permlib: #
pgRouting: CGAL
pingus: #
plasma-desktop:
plasma-workspace:
player: source-highlight #
plee-the-bear: bear-engine #
poedit: lucene++
pokerth:
polymake: #
povray: #
prusa-slicer: openvdb
psi4: #
ptlib: #
pulseeffects: #
pulseview: #
pyexiv2: #
python-lmiwbem: #
python-mapnik: mapnik #
python-tdlib:
qbittorrent: rb_libtorrent
qpid-cpp: #
qpid-qmf: #
rb_libtorrent: scratcharches=x86_64 #
rcsslogplayer: #
rcssmonitor: #
rcssserver: #
rcssserver3d: simspark
rdkit:
rigsofrods: mygui ogre
rmol: stdair airrac
rocs: #
rospack: #
rstudio: # GCC13 <cstdint> issue
scantailor:
schroot: #
scram:
scribus: #
scummvm-tools: #
sdcc: lyx
sdformat: urdfdom console-bridge
seqan: #
sevmgr: stdair
shiny: ogre
sigil: FlightCrew
sim: #
simcrs: rmol sevmgr airtsp simfqt airinv airrac
simfqt: stdair
simspark: #
sinfo: #
slic3r:
slic3r-prusa3d: scratcharches=x86_64 #
smesh: #
snapper: # ../snapper/Selinux.h:53:61: error: invalid conversion from 'const char*' to 'char*' [-fpermissive]
soci: #
sord: #
source-highlight: #
source-highlight-qt: source-highlight
sourcextractor++: elements-alexandria #
spring: scratcharches="x86_64 i686"
spring: source-highlight
springlobby: scratcharches=x86_64
springlobby: rb_libtorrent
srecord: #
stdair: # soci

stellarium: #
stp: cryptominisat

sumwars: ogre
supercollider:
supertux: #
swift: #
swig: #
sympol: #
syncevolution: akonadi
synfig: #
systemtap: dyninst #
tcpflow: #
teamgit: source-highlight source-highlight-qt
thrift: #
tomahawk: lucene++ # librados2
tracker: exempi
trademgen: stdair sevmgr
trafficserver: #
travelccm: stdair
tvlsim: airinv rmol simfqt simcrs
uhd: #
umbrello: #
urdfdom: console-bridge
urg:
usd: imath openshadinglanguage openvdb OpenImageIO #
uwsgi: mongodb mongo-cxx-driver #  device-mapper-persistent-data
valyriatear: #
vdrift:
vegastrike:
vfrnav: # @echo blocked by GCC update, requiring hdf5 and octave to be rebuilt @false
vigra: #
votca: espresso #
# votca-csg: votca-tools #
# votca-xtp: votca-csg #
vsomeip3: source-highlight
vsqlite++: # @echo blocked by GCC update, requiring hdf5 and openmpi to be rebuilt @false
vtk: #
websocketpp: #
wesnoth: #
widelands: # src/build_info.h:25:11: error: 'uint16_t' does not name a type GCC13 <cstdint> issue
writerperfect: #
xmlcopyeditor: #
xboxdrv: #
xmms2: #
xs: #
xsd: libcutl
xylib: #
yadex: #
yaml-cpp: #
yoshimi: #
zookeeper: #
zorba: #

#
# And now the rules to do the actual builds ...
#

# clone the repo if not already present
$(scmdir)/%/. :
	cd $(scmdir) && fedpkg clone rpms/$*


# bump the revision in the spec file
$(scmdir)/%/BUMP.$(id) : | $(scmdir)/%/.
	set -e ; \
	test -f dead.package && exit 1 ; \
	cd $(scmdir)/$* ; \
	test -f BUMP.$(id) && exit 0 ; \
	git checkout rawhide ; \
	git diff --quiet ; \
	git pull  --ff-only ; \
	rpmdev-bumpspec -c '$(comment)' $*.spec ; \
	if grep '^Release:[[:space:]]*%autorelease' $*.spec; then \
	  git commit --allow-empty -m '$(comment)' ; \
	else \
	  git commit -a -m '$(comment)' ; \
	fi
	@touch $@

# create SRPM and submit for scratch-build
$(scmdir)/%/SCRATCH.$(id) : | $(scmdir)/%/BUMP.$(id)
	set -e ; \
	cd $(scmdir)/$* ; \
	test -f SCRATCH.$(id) && exit 0 ; \
	fedpkg srpm ; \
	$(scratchbuild) --srpm `fedpkg verrel`.src.rpm > .$(notdir $@)
	@mv $(dir $@)/.$(notdir $@) $@

# push spec file changes and do real build
# In memory of Wayne Static...you
$(scmdir)/%/PUSHIT.$(id) : $(scmdir)/%/BUMP.$(id)
	git -C $(scmdir)/$* push
	touch $@

# yeah, you push it (spec file changes) and do real build
$(scmdir)/%/BUILD.$(id) : | $(scmdir)/%/PUSHIT.$(id)
	set -e ; \
	cd $(scmdir)/$* ; \
	if ! [ -f SCRATCH.$(id) ]; then \
		echo ; echo "$*: scratch-build not run yet" >&2 ; echo ; \
		$(_WITHOUT_SCRATCH) exit 1 ; \
	fi ; \
	fedpkg build --target $(target) --fail-fast; \
	koji wait-repo --build `fedpkg verrel` --target $(target) > .$(notdir $@)
	@mv $(dir $@)/.$(notdir $@) $@

SRPM_BASE_URL := https://www.mirrorservice.org/sites/dl.fedoraproject.org/pub/fedora/linux/development/rawhide/source/SRPMS
LATEST_SRPM = $(shell koji latest-pkg rawhide $(1) | awk 'NR==3{printf "${SRPM_BASE_URL}/%s/%s.src.rpm", tolower(substr($$1, 1, 1)), $$1}')

$(scmdir)/%/COPR.$(id) :
	copr-cli build -r fedora-rawhide-x86_64 Boost-beta "$(call LATEST_SRPM,$*)" > .$(notdir $@)
	@mv $(dir $@)/.$(notdir $@) $@

% :
	@$(MAKE) $(scmdir)/$@/$(ACTION).$(id)
	@echo $@: $(ACTION) complete
	@echo
	@echo $(ACTION) > $@


ifneq ($(build_id),)
BASEREPO:
	echo $(baserepos)/$(build_id) > $@
else
repo.json:
	$(FETCHURL) $(baserepos)/latest/repo.json

BASEREPO: repo.json
	python -c 'import sys, json; print("$(baserepos)/%s" % json.load(sys.stdin)["id"])' < $< > $@.tmp
	mv $@.tmp $@
endif

.INTERMEDIATE: BASEREPO repo.json

repo:
	mkdir -p repo

mock.cfg.in:
	@echo "No mock.cfg.in file found"
	@echo "See https://gitlab.com:jonathan-wakely/fedora-boost-rebuilds.git"
	@false

mock.cfg: mock.cfg.in BASEREPO repo
	m4 -P -D_ID_=$(id) -D_RELEASEVER_=$(releasever) -D_REPODIR_=file://$(CURDIR)/repo -D_BASEREPO_=$(shell cat BASEREPO) $< > $@
	@echo Copy the new boost RPMs into $(CURDIR)/repo and run 'createrepo $(CURDIR)/repo'

# create SRPM, build it, and add to local repo used by mock.cfg
# Could use --uniqueext=-$* arg to use unique chroots and allow parallel make.
$(scmdir)/%/LOCAL.$(id) : | $(scmdir)/%/BUMP.$(id) mock.cfg
	if [[ "$(MAKEFLAGS)" =~ '-j' ]] ; then false \
	error: cannot do LOCAL builds in parallel \
	; fi
	set -e ; \
	cd $(scmdir)/$* ; \
	test -f LOCAL.$(id) && exit 0 ; \
	fedpkg srpm ; \
	mock -r $(CURDIR)/mock.cfg --rebuild `fedpkg verrel`.src.rpm ; \
	mv /var/lib/mock/rawhide-$(id)-x86_64/result/*.rpm $(CURDIR)/repo/ ; \
	createrepo --update $(CURDIR)/repo
	@touch $@

.SECONDARY:

# This is specific to the Boost 1.73.0 rebuilds, which coincide with Python 3.9 rebuilds:
#include /home/jwakely/src/fedora-scm/UPDATES/boost-173/OVERLAP

# After the side tag is merged back, this query shows the signing queue:
# https://koji.fedoraproject.org/koji/builds?start=0&tagID=18679&order=nvr&inherited=0&latest=1
